import React from "react";
import "../Styles/Header.css";

const Header = () => {
  return (
    <>
      <div className="header">
        <p>
          We are upgrading to the Session Network and migrating to Session
          Token.
        </p>
        <button>Learn More</button>
      </div>
    </>
  );
};
export default Header;
