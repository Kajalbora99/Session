import React from "react";
import "../Styles/Navbar.css";

const Navbar = () => {
  return (
    <>
      <div className="navbar">
        <img src="Assets/logo.webp" alt="logo" />
        <div className="pages">
          <a href=" ">GITHUB</a>
          <a href=" ">BLOG</a>
          <a href=" ">TECHNICALS</a>
          <a href=" ">HELP</a>
          <a href=" ">DONATE</a>
          <button>DOWNLOAD</button>
        </div>
      </div>
    </>
  );
};
export default Navbar;
