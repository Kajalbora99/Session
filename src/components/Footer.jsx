import React from "react";
import "../Styles/Footer.css";

const Navbar = () => {
  return (
    <>
      <div className="section-5">
        <div>
          <h1>Friends don’t let friends use compromised messengers.</h1>
          <p>Sign up to the mailing list and start taking action!</p>
        </div>
        <form action="" method="get">
          <input type="text" placeholder="Your Email" required />
          <br />
          <button>Sign up</button>
        </form>
      </div>
      <div className="footer">
        <div className="one">
          <h3>Join the movement to keep the internet private!</h3>
          <p>
            Chat with like-minded individuals in the{" "}
            <a href="https://getsession.org/community">Session Community.</a>{" "}
          </p>
        </div>
        <div className="second">
          <div className="two">
            <div className="two1">
              <div className="about">
                <h3>ABOUT</h3>
                <a href="/">Whitepaper</a>
                <br />
                <a href="/">Privacy Policy</a>
                <br />
                <a href="/">Terms of Service</a>
                <br />
                <a href="/">Blog</a>
                <br />
                <a href="/">FAQ</a>
              </div>
              <div className="company">
                <h3>COMPANY</h3>
                <a href="/">OPTF</a>
                <br />
                <a href="/">Oxen</a>
                <br />
                <a href="/">Lokinet</a>
                <br />
                <a href="/">Media Kit</a>
                <br />
                <a href="/">Transparency Report</a>
              </div>
            </div>
            <div className="two2">
              <div className="links">
                <h3>LINKS</h3>
              </div>
              <div className="support">
                <h3>SUPPORT</h3>
                <a href="/">Visit our Help Centre</a>
              </div>
            </div>
          </div>
          <div className="three">
            <img src="Assets/logo-white.png" alt="logo" />
            <p id="p2">
              Session is an <span> end-to-end </span> encrypted messenger that
              removes <span> sensitive </span> metadata,{" "}
              <span> designed and built </span> for people who want privacy and
              freedom from <span> any form of </span> surveillance.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};
export default Navbar;
